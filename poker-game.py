import fileinput

def poker(hands):
    """
    Return the max of the hands. The rank of the hands decided by hand_rank
    :param hands: the list of hands of two players
    :return: the wining hand
    """
    return max(hands, key=hand_rank)


def hand_rank(hand):
    """
    
    Return a value indicating the ranking of the hand. ex. if it is a straight and flush, it would
    return a tuple (handrank for straight flush, tie breaker which is the highest card used in straight)
    :param hand: the hand of a player
    :return: a tuple (handrank,tie breaker)

    For tie breakers, I refered: https://www.adda52.com/poker/poker-rules/cash-game-rules/tie-breaker-rules
    """
    
    ranks = card_ranks(hand)

    if royal(ranks) and flush(hand): 
        return 9
    elif straight(ranks) and flush(hand):
        return (8,max(ranks))
    elif kind(ranks,4):
        return (7,kind(ranks,4),kind(ranks,1))
    elif kind(ranks,3) and kind(ranks,2):
        return (6,kind(ranks,3),kind(ranks,2))
    elif flush(hand):           
        return (5,ranks)
    elif straight(ranks):
        return (4,max(ranks))
    elif kind(ranks,3):
        return (3,kind(ranks,3),ranks)
    elif two_pairs(ranks):
        return (2,max(two_pairs(ranks)))
    elif kind(ranks,2):
        return (1,kind(ranks,2),ranks)
    else:
        return (0,ranks)


def card_ranks(hand):
    """
    :param hand: the hand of the player
    :return list of ranks (card value) sorted in descending order
    """
    higher_cards  = {"T":10,"J":11,"Q":12,"K":13,"A":14}
    ranks = []
    for each in hand:
        if each[0] in higher_cards:
            ranks.append(higher_cards[each[0]])
        else:
            ranks.append(int(each[0]))
    return sorted(ranks,reverse=True)

def royal(ranks):
    '''
    An Ace-High Straight Flush. Example: TH JH QH KH AH. 
    :param ranks: value of the card
    :return True: if royal flush else False
    '''
    royal_list = [10,11,12,13,14]
    if ranks == royal_list:
        return True
    else:
        return False


def straight(ranks):
    '''
    card rank consecutive. Example: 5D 6H 7H 8C 9H. 
    :param ranks: value of the card
    :return True: if straight else False
    '''
    is_straight = False
    for i in range(len(ranks)-1,0,-1):
        sum = ranks[i]+1
        next_val = ranks[i-1]
        if sum==next_val:
            is_straight=True
        else:
            is_straight=False
            break
    return is_straight


def flush(hand):
    '''
    card suit is same. Example: 5D 7D 7D 8D 9D. 
    :param hand: entire hand of the player
    :return True if flush else False
    '''
    flushed = set()
    for each in hand:
        flushed.add(each[1])
    if len(flushed)==1:
        return True
    else:
        return False


def kind(ranks,n):
    '''
    n card value is same. Example: 4 of a kind: AS AD AH AH KC. 
    :param ranks: value of the card
    :param n: number of equal cards looking for. ex. To check 4 of a kind, n=4
    :return the value which is repeating n times. else None if not Kind
    '''
    for each in ranks:
        if ranks.count(each)==n:
            return each
    return None


def two_pairs(ranks):
    '''
    two pairs of cards same. Example: 5D 5C 9H 9D QH. 
    :param ranks: value of the card
    :return tuple of numbers which are in a pair. ex.(9,5). else None if no two pairs exist
    '''
    two_pair_list = []
    for each in set(ranks):
        if ranks.count(each)==2:
            two_pair_list.append(each)
    if len(two_pair_list)==2:
        return tuple(two_pair_list)
    return None

with fileinput.input(files="poker-hands.txt") as f:
    p1_wins = 0
    p2_wins = 0
    for lines in f:
        hands = lines.split()
        player1_hand = hands[:5]
        player2_hand = hands[5:]
        wining_hand = poker([player1_hand,player2_hand])
        if wining_hand == player1_hand:
            p1_wins = p1_wins + 1
        else:
            p2_wins = p2_wins + 1


    print("Player 1:",p1_wins)
    print("Player 2:", p2_wins)
    






